FROM python:3.8.2-alpine

COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

WORKDIR /backend4nginx

COPY src .

EXPOSE 8080/tcp

CMD ["python", "main.py"]
