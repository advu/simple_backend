# CHANGELOG

## 0.1.2

1. Added **sleep** parameter in GET request: `http://127.0.0.1:8080/?sleep=5`

## 0.1.1

1. Apped environment variable for response text: `SIMPLE_BACKEND_RESPONSE`

## 0.1.0

1. The first server implementation to count the number of requests.
