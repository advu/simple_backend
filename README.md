# Simple backend

Simple backend for counting the nubmer of requests.

Image on docker hub: https://hub.docker.com/r/advu/simple_backend

Build:

```bash
docker build -t advu/simple_backend:$(cat VERSION) .
```

Run:

```bash
docker run -p 8080:8080 advu/simple_backend:$(cat VERSION)
```

Change response text with environment variable:
```bash
docker run -p 8080:8080 -e SIMPLE_BACKEND_RESPONSE='Your response text here!' advu/simple_backend:$(cat VERSION)
```

Sleep:
```
curl http://127.0.0.1:8080/?sleep=5
```
