#!/usr/bin/python3

import asyncio
from aiohttp import web
import os

ENV_VAR_RESPONSE_NAME = "SIMPLE_BACKEND_RESPONSE"

async def root(request):
    request.app["metrics"]["request_count"] += 1
    if "sleep" in request.query:
        sleep = request.query.get("sleep")
        if sleep.isdigit():
            await asyncio.sleep(int(sleep))
    if ENV_VAR_RESPONSE_NAME in os.environ:
        response_text = os.environ.get(ENV_VAR_RESPONSE_NAME)
    else:
        response_text = "Request received"
    return web.Response(text=response_text)

async def metrics(request):
    metrics_tmpl = "backend_request_count {}"
    metrics = metrics_tmpl.format(request.app["metrics"]["request_count"])
    return web.Response(text=metrics)

if __name__ == "__main__":
    app = web.Application()
    app.add_routes([
        web.get("/", root),
        web.get("/metrics", metrics),
    ])
    app["metrics"] = {
        "request_count": 0
    }
    web.run_app(app)
